README
======

Description
-----------------
ObjectArrayValueAverager is a simple library to take an array of objects containing values, and average those values into an output object.

Dependencies
-----------------
...

Example
----------------
...

Contributing
----------------
If you want to change or to improve something, feel free to fork it and send me pull requests
