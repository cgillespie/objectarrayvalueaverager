<?php

namespace ObjectArrayValueAverager;

class Averager
{
    protected $sourceObjectArray;
    protected $outputObject;
    protected $fields = array();
    protected $applyRounding = true;
    protected $roundingPrecision = 0;

    public function calculateAverage()
    {
        $this->calculateRunningAverageForAllFields();

        if ( $this->getApplyRounding() ) {
            $this->roundAllFields();
        }  

        return $this->getOutputObject();
    }

    protected function calculateRunningAverageForAllFields() 
    {
        $output = $this->getOutputObject();

        foreach ( $this->getFields() as $field ) {

            $getter = $this->getFieldAccessorName($field);
            $setter = $this->getFieldModifierName($field);

            $i = 0;
            foreach ( $this->getSourceObjectArray() as $instance ) {
                $runningAverage = (($output->$getter() * $i) + $instance->$getter()) / ($i + 1);
                $output->$setter($runningAverage);
                $i++;
            }
        }        
    }

    protected function roundAllFields() 
    {
        $output = $this->getOutputObject();

        foreach ( $this->getFields() as $field ) {
            $getter = $this->getFieldAccessorName($field);
            $setter = $this->getFieldModifierName($field);

            $output->$setter(round($output->$getter(), $this->getRoundingPrecision()));
        }
    }

    protected function getFieldAccessorName($field) 
    {
        return 'get' . ucfirst($field);
    }

    protected function getFieldModifierName($field) 
    {
        return 'set' . ucfirst($field);
    }

    /**
     * Gets the value of sourceObjectArray.
     *
     * @return mixed
     */
    public function getSourceObjectArray()
    {
        return $this->sourceObjectArray;
    }

    /**
     * Sets the value of sourceObjectArray.
     *
     * @param mixed $sourceObjectArray the source object array
     *
     * @return self
     */
    public function setSourceObjectArray($sourceObjectArray)
    {
        $this->sourceObjectArray = $sourceObjectArray;

        return $this;
    }

    /**
     * Gets the value of outputObject.
     *
     * @return mixed
     */
    public function getOutputObject()
    {
        return $this->outputObject;
    }

    /**
     * Sets the value of outputObject.
     *
     * @param mixed $outputObject the output object
     *
     * @return self
     */
    public function setOutputObject($outputObject)
    {
        $this->outputObject = $outputObject;

        return $this;
    }

    /**
     * Gets the value of fields.
     *
     * @return mixed
     */
    public function getFields()
    {
        return $this->fields;
    }

    /**
     * Sets the value of fields.
     *
     * @param mixed $fields the fields
     *
     * @return self
     */
    public function setFields($fields)
    {
        $this->fields = $fields;

        return $this;
    }

    /**
     * Gets the value of applyRounding.
     *
     * @return mixed
     */
    public function getApplyRounding()
    {
        return $this->applyRounding;
    }

    /**
     * Sets the value of applyRounding.
     *
     * @param mixed $applyRounding the apply rounding
     *
     * @return self
     */
    public function setApplyRounding($applyRounding)
    {
        $this->applyRounding = $applyRounding;

        return $this;
    }

    /**
     * Gets the value of roundingPrecision.
     *
     * @return mixed
     */
    public function getRoundingPrecision()
    {
        return $this->roundingPrecision;
    }

    /**
     * Sets the value of roundingPrecision.
     *
     * @param mixed $roundingPrecision the rounding precision
     *
     * @return self
     */
    public function setRoundingPrecision($roundingPrecision)
    {
        $this->roundingPrecision = $roundingPrecision;

        return $this;
    }
}