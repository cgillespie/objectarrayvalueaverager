<?php

namespace Test\ObjectArrayValueAverager;

use \ObjectArrayValueAverager\Averager;

class SimpleObject
{
    protected $A;
    protected $B;

    /**
     * Gets the value of A.
     *
     * @return mixed
     */
    public function getA()
    {
        return $this->A;
    }

    /**
     * Sets the value of A.
     *
     * @param mixed $A the a
     *
     * @return self
     */
    public function setA($A)
    {
        $this->A = $A;

        return $this;
    }

    /**
     * Gets the value of B.
     *
     * @return mixed
     */
    public function getB()
    {
        return $this->B;
    }

    /**
     * Sets the value of B.
     *
     * @param mixed $B the b
     *
     * @return self
     */
    public function setB($B)
    {
        $this->B = $B;

        return $this;
    }
}

class AveragerTest extends \PHPUnit_Framework_TestCase
{
    function getterAndSetterDataProvider()
    {
        return array(
            array('sourceObjectArray', 'TEST'),
            array('outputObject', 'TEST2'),
            array('fields', 'TEST3'),
            array('applyRounding', 'TEST4'),
            array('roundingPrecision', 'TEST5')
        );
    }

    /**
     * @dataProvider getterAndSetterDataProvider
     */
    function testGettersAndSetters($field, $value)
    {
        $averager = new Averager();

        $setter = 'set' . ucfirst($field);
        $getter = 'get' . ucfirst($field);

        $averager->$setter($value);

        $this->assertEquals($averager->$getter(), $value);
    }

    function calculateAverageDataProvider()
    {
        return array(
            array(5,98,3,1,4,50,0),
            array(5,98,3,1,4,49.5,1)
        );
    }

    /**
     * @dataProvider calculateAverageDataProvider
     */
    function testCalculateAverage($a1, $b1, $a2, $b2, $c1, $c2, $precision) 
    {
        $object1 = new SimpleObject();
        $object1->setA($a1)
            ->setB($b1);

        $object2 = new SimpleObject();
        $object2->setA($a2)
            ->setB($b2);

        $object3 = new SimpleObject();            

        $averager = new Averager();
        $output = $averager
            ->setSourceObjectArray(array($object1, $object2))
            ->setOutputObject($object3)
            ->setFields(array('A','B'))
            ->setRoundingPrecision($precision)
            ->calculateAverage();
         
        $this->assertEquals($c1, $output->getA());
        $this->assertEquals($c2, $output->getB());       
    }
}